variable "network_address_space" {
}

resource "aws_vpc" "vpc" {
  cidr_block = "${var.network_address_space}"
}

output "aws_vpc_id" {
  value = "${aws_vpc.vpc.id}"
}
