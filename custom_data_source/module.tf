variable "aws_access_key" {
}
variable "aws_secret_key" {

}

data "external" "configuration" {
    program = ["bash", "script.sh"]
}

data "aws_ami" "aws_linux" {

    owners = ["self"]

    filter {
        name   = "name"
        values = ["nginx-web-image"]
    }

}

provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "us-west-1"
}

resource "aws_instance" "nginx" {
    count = "${data.external.configuration.result.machine}"
    ami = "${data.aws_ami.aws_linux.id}"
    instance_type = "t2.micro"
    key_name = "asura-key-california"
    vpc_security_group_ids = ["sg-5d468121"]

    tags = {
        Name = "Custom-data-source"
    }

}