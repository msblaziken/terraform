variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "network_address_space" {
   default = "10.3.0.0/16"
}

variable "network_address_subnet_1" {
  default = "10.3.1.0/24"
}

data "aws_availability_zones" "available" {}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-east-1"
}

resource "aws_vpc" "vpc" {
  cidr_block = "${var.network_address_space}"
}

resource "aws_subnet" "subnet1" {
  cidr_block = "${var.network_address_subnet_1}"
  vpc_id = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
}