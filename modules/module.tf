variable "network_address_space" {
  default = "172.4.0.0/16"
}

variable "network_subnet_space" {}

variable "aws_access_key" {
}
variable "aws_secret_key" {

}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-west-1"
}

module "vpc" {
  source = "./vpc"
  network_address_space = "${var.network_address_space}"
}

resource "aws_subnet" "subnet1" {
  cidr_block = "${var.network_subnet_space}"
  vpc_id = "${module.vpc.aws_vpc_id}"
}