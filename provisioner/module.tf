variable "aws_access_key" {
}
variable "aws_secret_key" {

}
variable "pem_file" {}
variable "instances_number" {
  default = 1
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-west-1"
}

resource "aws_instance" "nginx" {
  count = "${var.instances_number}"
  ami = "ami-08f5bf12aaac5e758"
  instance_type = "t2.micro"
  key_name = "asura-key-california"
  vpc_security_group_ids = ["sg-5d468121"]

  connection {
    host = self.public_ip
    type = "ssh"
    user = "ec2-user"
    private_key = "${file(var.pem_file)}"
  }

  provisioner "remote-exec" {
     inline = ["sudo yum install -y nmap"]
  }

}

output "aws_public_ip_1" {
  value = "${aws_instance.nginx[0].public_ip}"
}

output "aws_public_ip_2" {
  value = "${aws_instance.nginx[1].public_ip}"
}


