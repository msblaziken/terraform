variable "aws_access_key_id" {}
variable "aws_secret_key_id" {}
variable "profile" {}

data "template_file" "asura" {
  template = "${file("aws_config.tpl")}"

  vars = {
    profile = "${var.profile}"
    aws_access_key_id = "${var.aws_access_key_id}"
    aws_secret_key_id = "${var.aws_secret_key_id}"
  }

}

resource "local_file" "aws_keys" {
  content = "${data.template_file.asura.rendered}"
  filename = "credentials2"
}

