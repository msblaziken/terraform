variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "region_name" {}
variable "ami_name" {}


provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.region_name}"
}

resource "aws_instance" "nginx" {
  ami = "${var.ami_name}"
  instance_type = "t2.micro"

}


