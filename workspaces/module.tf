variable "aws_access_key" {
}
variable "aws_secret_key" {

}
variable "instances_number" {
  default = 1
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-west-1"
}

resource "aws_instance" "nginx" {
  count = "${var.instances_number}"
  ami = "ami-08f5bf12aaac5e758"
  instance_type = "t2.micro"
  key_name = "asura-key-california"
  vpc_security_group_ids = ["sg-5d468121"]

  tags = {
    Name = "Terraform-${terraform.workspace}"
  }

}